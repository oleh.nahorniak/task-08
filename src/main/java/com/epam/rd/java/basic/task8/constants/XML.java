package com.epam.rd.java.basic.task8.constants;

public enum XML {

    CARS("cars"),
        CAR("car"),
            NAME("name"),
            ORIGIN("origin"),
            TYPE("type"),
            YEAR("year"),

            SPECIFICATIONS("specifications"),
                PRICE("price"),

                ENGINE("engine"),
                    POWER("power"),
                    TORQUE("torque"),

                DIMENSIONS("dimensions"),
                    LENGTH("length"),
                    WIDTH("width"),
                    HEIGHT("height"),
                    PASSENGER_VOLUME("passengerVolume"),
                    CARGO_VOLUME("cargoVolume"),

                FUEL_CONSUMPTIONS("fuelConsumptions"),
                    GENERAL("general"),
                    CITY("city"),
                    HIGHWAY("highway");

    private String value;

    XML(final String value) {
        this.value = value;
    }

    public boolean equalsTo(final String name) {
        return value.equals(name);
    }

    public final String value() {
        return value;
    }
}
