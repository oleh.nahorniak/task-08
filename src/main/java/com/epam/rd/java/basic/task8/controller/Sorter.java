package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entities.Car;
import com.epam.rd.java.basic.task8.entities.Cars;

import java.util.Collections;
import java.util.Comparator;

public final class Sorter {

    public static final Comparator<Car>
    SORT_CARS_BY_NAME = (first, second) -> {
        String carName1 = first.getName();
        String carName2 = second.getName();
        return carName1.compareTo(carName2);
    };

    public static final Comparator<Car>
    SORT_CARS_BY_PRICE = (first, second) -> {
        Integer carPrice1 = first.getSpecifications().getPrice();
        Integer carPrice2 = second.getSpecifications().getPrice();
        return carPrice1.compareTo(carPrice2);
    };

    public static final Comparator<Car>
    SORT_CARS_BY_ENGINE_POWER = (first, second) -> {
        Integer enginePower1 = first.getSpecifications().getEngine().getPower();
        Integer enginePower2 = second.getSpecifications().getEngine().getPower();
        return enginePower1.compareTo(enginePower2);
    };

    public static void sortCarsByName(Cars cars) {
        cars.getCars().sort(SORT_CARS_BY_NAME);
    }

    public static void sortCarsByPrice(Cars cars) {
        cars.getCars().sort(SORT_CARS_BY_PRICE);
    }

    public static void sortCarsByEnginePower(Cars cars) {
        cars.getCars().sort(SORT_CARS_BY_ENGINE_POWER);
    }

    private Sorter() {

    }


}
